# frozen_string_literal: true

require 'erb'

RUBY_VERSIONS = ENV.fetch('RUBY_VERSIONS').split(' ').freeze
IMAGE_PURPOSE = %w[builder runtime].freeze
BASE_IMAGES=%w[alpine slim].freeze
RUBY_PRESETS = [
  {},
  { purpose: 'runtime' },
  { purpose: 'builder' },
  { purpose: 'runtime', pg: true },
  { purpose: 'builder', pg: true }
].freeze

erb = ERB.new(File.read('ruby-build.yaml.erb'), nil, '-')

begin
  file = File.open('ruby-ci.yml', 'w')
  file << erb.result
rescue IOError => e
  # some error occur, dir not writable etc.
  p e
ensure
  file&.close
end
